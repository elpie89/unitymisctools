using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class EditorUtils
{

    public static void ReplaceObjects(GameObject replaceObj, GameObject[] selectedObj, bool position, bool rotation, bool scale)
    {
        foreach (GameObject curObj in selectedObj)
        {
            GameObject newObj = (GameObject)GameObject.Instantiate(replaceObj, Vector3.zero, Quaternion.identity);
            newObj.transform.rotation = replaceObj.transform.rotation;
            newObj.transform.localScale = replaceObj.transform.localScale;
            int index = newObj.name.IndexOf("Clone");
            newObj.name = newObj.name.Substring(0, index - 1);
            if (position)
            {
                newObj.transform.position = curObj.transform.position;
            }
            if (rotation)
            {
                newObj.transform.rotation = curObj.transform.rotation;
            }
            if (scale)
            {
                newObj.transform.localScale = curObj.transform.localScale;
            }
            GameObject.DestroyImmediate(curObj);
        }
        replaceObj = null;
    }

    public static void DuplicateInstances(GameObject originalObj, int horInst, int verInst, float offset)
    {

        Vector3 newPosition = new Vector3();

        newPosition = originalObj.transform.position;
        for (int i = 1; i < horInst; i++)
        {
            newPosition.x = originalObj.transform.position.x + (offset * i);
            GameObject newObj = (GameObject)GameObject.Instantiate(originalObj, newPosition, Quaternion.identity);
            newObj.transform.rotation = originalObj.transform.rotation;
            newObj.transform.localScale = originalObj.transform.localScale;
        }

        newPosition = originalObj.transform.position;
        for (int i = 1; i < verInst; i++)
        {
            newPosition.z = originalObj.transform.position.z + (offset * i);
            GameObject newObj = (GameObject)GameObject.Instantiate(originalObj, newPosition, Quaternion.identity);
            newObj.transform.rotation = originalObj.transform.rotation;
            newObj.transform.localScale = originalObj.transform.localScale;
        }
    }

    public static ParticleSystem.Particle[] GetParticleList(ParticleSystem mParticleSystem)
    {
        ParticleSystem.Particle[] pList = new ParticleSystem.Particle[mParticleSystem.particleCount];
        return pList;
    }

    public static void ScaleParticles(GameObject userObj, float scaleAmount)
    {
        if (userObj.GetComponent<ParticleSystem>())
        {
            ParticleSystem rootParticle;
            ParticleSystem[] childrenParticle = new ParticleSystem[0];
            ParticleSystem.Particle[] particleList;

            rootParticle = userObj.GetComponent<ParticleSystem>();
            childrenParticle = rootParticle.GetComponentsInChildren<ParticleSystem>();

            particleList = GetParticleList(rootParticle);
            rootParticle.GetParticles(particleList);


            for (int i = 0; i < particleList.Length; i++)
            {
                float lifePorcent = particleList[i].lifetime / particleList[i].startLifetime;
                particleList[i].size = (Mathf.Lerp(1000, particleList[i].size, lifePorcent));
            }

            foreach (ParticleSystem item in childrenParticle)
            {
                float iStartSize = item.startSize;

                iStartSize = (iStartSize * scaleAmount) / 100;
                item.startSize = iStartSize;
            }
        }
        else
        {
            Debug.Log("L'oggetto deve contenere un componente particellare");
        }
    }

    public static Component[] ListComponents(GameObject obj)
    {
        Component[] list = obj.GetComponents<Component>();
        return list;
    }

    public static MonoBehaviour[] ListMono(GameObject obj)
    {
        MonoBehaviour[] list = obj.GetComponents<MonoBehaviour>();
        return list;
    }

    public static void MultyCopyComponents(GameObject sourceObj, GameObject destinationObj, bool[] copyComp, bool all)
    {
        Component[] list = ListComponents(sourceObj);

        for (int i = 0; i < list.Length; i++)
        {
            if (copyComp[i] && !all)
            {
                UnityEditorInternal.ComponentUtility.CopyComponent(list[i]);
                UnityEditorInternal.ComponentUtility.PasteComponentAsNew(destinationObj);
            }
            else if (all)
            {
                UnityEditorInternal.ComponentUtility.CopyComponent(list[i]);
                UnityEditorInternal.ComponentUtility.PasteComponentAsNew(destinationObj);
            }
        }

    }

    public static void ClearComponents(GameObject obj, bool[] mantainsComp, bool all)
    {
        Component[] list = ListComponents(obj);

        for (int i = 0; i < list.Length; i++)
        {
            if (mantainsComp[i] && !all)
            {
                GameObject.DestroyImmediate(list[i]);
            }
            else if (all)
            {
                GameObject.DestroyImmediate(list[i]);
            }
        }
    }

    public static List<Component> ExistingComponents(List<GameObject> objList)
    {
        List<Component> componentList = new List<Component>();

        foreach (GameObject obj in objList)
        {
            Component[] componentArray = ListComponents(obj);

            foreach (Component comp in componentArray)
            {

                if (!componentList.Exists(x => x.GetType() == comp.GetType()))
                {
                    componentList.Add(comp);
                }
            }
        }

        return componentList;
    }

    public static bool CheckIfObjHasComponent(GameObject obj, List<Component> toCheck)
    {
        List<Component> present = ListComponents(obj).ToList();

        for (int i = 0; i < present.Count; i++)
        {
            bool expression = toCheck.Any(item => item.GetType() == present.ElementAt(i).GetType());
            if (!expression)
            {
                return false;
            }
        }
        return true;
    }

    public static bool CheckIfObjHasOneOfComponent(GameObject obj, List<Component> toCheck)
    {
        List<Component> present = ListComponents(obj).ToList();


        for (int i = 0; i < toCheck.Count; i++)
        {
            bool expression = present.Exists(item => item.GetType() == toCheck.ElementAt(i).GetType());
            if (expression) return true;
        }
        return false;
    }

    public static bool CheckIfObjHasThisComponent(GameObject obj, List<Component> toCheck)
    {
        List<Component> present = ListComponents(obj).ToList();

        for (int i = 0; i < toCheck.Count; i++)
        {
            bool expression = present.Exists(item => item.GetType() == toCheck.ElementAt(i).GetType());
            if (!expression) return false;
        }
        return true;
    }

    public static void GroupObject(List<GameObject> list, GameObject grp)
    {
        foreach (GameObject obj in list)
        {
            Undo.SetTransformParent(obj.transform, grp.transform, null);
        }
    }

    public static void SetParent(GameObject child, GameObject father)
    {
        child.transform.parent = father.transform;
    }
}
