using UnityEngine;
using UnityEditor;
using System.Collections;

public class ObjectDuplicatorWindow : EditorWindow
{

    static ObjectDuplicatorWindow curWindow;
    GameObject userDefObj;

    bool mRotation = true;
    bool mScale = true;
    bool mPosition = true;
    int selectionCount = 0;
    int horizontalInstances = 0;
    int verticalInstances = 0;
    float offsetInstances = 0;
    GameObject[] selectedObj = new GameObject[0];

    #region Main methods
    [MenuItem("Luca's Tool/ObjDuplicator")]
    static void Init()
    {
        curWindow = (ObjectDuplicatorWindow)EditorWindow.GetWindow(typeof(ObjectDuplicatorWindow));
        curWindow.titleContent = new GUIContent("Obj Duplicator");
        curWindow.minSize = new Vector2(436, 300);
        curWindow.maxSize = new Vector2(436, 600);
    }

    void OnGUI()
    {
        selectedObj = Selection.gameObjects;
        selectionCount = selectedObj.Length;

        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();

        GUILayout.Label("Duplicator Properties:", EditorStyles.boldLabel);
        //object field
        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        GUILayout.BeginVertical();
        userDefObj = (GameObject)EditorGUILayout.ObjectField("Set matrix obj", userDefObj, typeof(GameObject), true);
        GUILayout.EndVertical();
        GUILayout.Space(10);
        GUILayout.EndHorizontal();

        //label counter
        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("Number of object selected: " + selectionCount);
        GUILayout.Space(10);
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("If not cheched the new obj mantains the matrix attribute  ", EditorStyles.boldLabel);
        GUILayout.Space(10);
        GUILayout.EndHorizontal();

        //toggle definition
        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        mPosition = (bool)EditorGUILayout.Toggle("Position", mPosition);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        mRotation = (bool)EditorGUILayout.Toggle("Rotation", mRotation);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        mScale = (bool)EditorGUILayout.Toggle("Scale", mScale);
        GUILayout.EndHorizontal();



        if (selectionCount > 0 && userDefObj)
        {
            if (GUILayout.Button("Replace", GUILayout.Height(40)))
            {
                EditorUtils.ReplaceObjects(userDefObj, selectedObj, mPosition, mRotation, mScale);
            }
        }


        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        horizontalInstances = (int)EditorGUILayout.IntField("Horizontal Instances", horizontalInstances);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        verticalInstances = (int)EditorGUILayout.IntField("Vertical Instances", verticalInstances);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        offsetInstances = (int)EditorGUILayout.FloatField("Offset", offsetInstances);
        GUILayout.EndHorizontal();

        if ((horizontalInstances > 0 || verticalInstances > 0) && userDefObj)
        {
            if (GUILayout.Button("Duplicate Instances", GUILayout.Height(40)))
            {
                EditorUtils.DuplicateInstances(userDefObj, horizontalInstances, verticalInstances, offsetInstances);
            }
        }


        GUILayout.Space(10);
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();


        Repaint();
    }
    #endregion

}
